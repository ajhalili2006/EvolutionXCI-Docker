# Evolution X Over-the-Air CI
This repository is where the Docker image builds for the Evolution X OTA CI in a Dockerized form.

## What you need?
In order to make a successful deployment of this Docker image to Docker Registry, you need to have these:

**Variables**
* `DOCKER_USERNAME` and `DOCKER_PASSWORD`: Your Docker ID username and the access token with permissions to push images to Docker Registry.
* `TELEGRAM_TOKEN`:
* `TELEGRAM_CHATID`: Target chat ID in Telegram for notifying build updates. If the logic code doesn't matches what defined in code, it falls back to `-1001176435322` which only @RealAkito notified. Edit the `gitlab_ci-build.sh` after you fork this repository and change these:
```bash
## From L16 to L23

## Do some logic on checking chat IDs
if [ $TELEGRAM_CHATID == "your-chat-id-here" ]; then
    sendTG "\`Docker image is being pushed to GitLab Containner Registry!\`"
else
    echo "Falling back the default chat ID..."
    TELEGRAM_CHATID=-1001176435322
    sendTG "\`Docker image is being pushed to GitLab Containner Registry!\`"
fi

## From L30 to L35

## Notify <Your Name here> or Akito after successful image push.
if [ $TELEGRAM_CHATID == "your-chat-id-here" ]; then
    sendTG "@YourTGUsernameHere \`I have pushed new images to GitLab Containner Registry\` %0A [Images are Here](https://gitlab.com/your-username/EvolutionXCI-Docker/container_registry)"
else
    sendTG "@RealAkito \`I have pushed new images to GitLab Container Registry\` %0A [Images are Here](https://gitlab.com/HarukaNetwork/OSS/EvolutionXCI-Docker/container_registry)"
fi
```
## Deploying to Docker Registry
To trigger a `docker push` to `registry.docker.com`, create a commit ends/starts with `[PUSH TO DOCKER REGISTRY]` and push it here.
