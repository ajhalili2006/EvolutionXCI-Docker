#!/usr/bin/env bash
## Adopted from the original build.sh file.
## Now for GitLab Containner Registry in the 'master' branch.

## Define stuff by pulling them from env
## Mostly add them in GitLab Project Setting -> CI / CD -> Variables
TELEGRAM_TOKEN=$TELEGRAM_TOKEN
## We don't need them, because we're pushing images to GL Containner Registry instead of Docker Registry.
##DOCKER_PASSWORD=$DOCKER_PASSWORD
##DOCKER_USERNAME=$DOCKER_USERNAME

function sendTG() {
    /usr/bin/curl -s "https://api.telegram.org/bot${TELEGRAM_TOKEN}/sendmessage" --data "text=${*}&chat_id=${TELEGRAM_CHATID}&parse_mode=Markdown"
}

## Do some logic on checking chat IDs
if [ $TELEGRAM_CHATID == "705519392" ]; then
    sendTG "\`Docker image is being pushed to GitLab Containner Registry!\`"
else
    echo "Falling back the default chat ID..."
    TELEGRAM_CHATID=-1001176435322
    sendTG "\`Docker image is being pushed to GitLab Containner Registry!\`"
fi

docker build . -t "$CI_REGISTRY_IMAGE"
# No more
# echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
docker push "$CI_REGISTRY_NAME"

## Notify Andrei Jiroh or Akito after successful image push.
if [ $TELEGRAM_CHATID == "705519392" ]; then
    sendTG "@AJHalili2006 \`I have pushed new images to GitLab Containner Registry\` %0A [Images are Here](https://gitlab.com/AndreiJirohHaliliDev2006/EvolutionXCI-Docker/container_registry)"
else
    sendTG "@RealAkito \`I have pushed new images to GitLab Container Registry\` %0A [Images are Here](https://gitlab.com/HarukaNetwork/OSS/EvolutionXCI-Docker/container_registry)"
fi
